# -*- coding: UTF-8 -*-
'''
Updated on 2015-02-16 for Python 3.4
@author: Johnny Tsheke
'''
a=0
b=0
c=0
cOk=False

while(not cOk):
    #demande et validation de a
    aOk=False
    while(not aOk):
        try:
            a=input("Entrez un nombre reel (le dividende) svp")
            a=float(a)
            aOk=True # pour arreter la boucle car on a effectivement un nombre reel
        except:
            print(a," n'est pas un nombre réel valide")
    #demande et validation de b       
    bOk=False
    while(not bOk): 
        try:
            b=input("Entrez un nombre reel (le diviseur) svp")
            b=float(b)
            bOk=True
        except:
            print(b," n'est pas un nombre réel valide")
    #calcul de C avec controle d'exception (anomalie)
    try:
        c=a//b
        cOk=True
    except:
        print(a," et ", b, " ne sont pas valides pour calculer un quotient")
        print("on recomence tout")

print("Le dividende a= ",a)
print("Le diviseur b= ",b)
print("Le quotient c= ",c)
