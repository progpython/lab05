# -*- coding: UTF-8 -*-
'''
Created on 11 févr. 2014
Updated on 2014-10-16 for Python 3.4
@author: Johnny Tsheke
'''

codes=["H3A 3Z1","H2A 1L1", "H8T 3N1","H2A 5T1","H1B 8Q7"]

print("Voici les codes postaux valides")
print(codes)
code=input("Entrez un code postal valide svp")

while(not code in codes) :
    print(code, "n'est pas un code postal valide")
    code=input("Entrez un code postal valide svp")

print("le code accepté est :",code)