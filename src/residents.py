# -*- coding: UTF-8 -*-
'''
Updated on 2015-02-16 for Python 3.4
@author: Johnny Tsheke
'''

liste=[{"Prenom":"David"},{"Prenom":"Solange"},{"Prenom":"Celine"},{"Prenom":"Vincent"},{"Prenom":"Bruce"}]
codes=["H3A 3Z1","H2A 1L1", "H8T 3N1","H2A 5T1","H1B 8Q7"]

for res in liste:
    code=""
    while(code not in codes):
        print("Voici les codes postaux valides")
        print(codes)
        print("\nEntrez le postal de ",res["Prenom"]," svp\n")
        code=input()#ici on affiche rien car message deja affiché
    #a ce niveau on a un bon code postal
    # on met a jour l'enregistrement de la personne
    res.update({"Code":code})
# à ce niveau tous les enregistrement sont mis a jour.
print("Tous les enregistrements mis à jour !")
codeResidence=""
while(codeResidence not in codes):
    print("Voici les codes postaux valides")
    print(codes)
    codeResidence=input("Entrez un code postal valide pour voir la liste des résidents svp\n")

# à ce niveau on a le code postal. on va afficher les réseidents
print("Voici les résidents de ce code postal :",codeResidence)
print("--------------------------------")
for res in liste:
    if(res["Code"]==codeResidence):#La personne réside à ce code postal
        print("Prénom: ",res["Prenom"])
        print("Code Postal",res["Code"])
        print("--------------------------------")
#ici on déjà affiché tous les résident. on termine le programme
print("Fin du programme") 